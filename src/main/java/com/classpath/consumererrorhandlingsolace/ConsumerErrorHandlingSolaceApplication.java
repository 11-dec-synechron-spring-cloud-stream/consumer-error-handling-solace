package com.classpath.consumererrorhandlingsolace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerErrorHandlingSolaceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerErrorHandlingSolaceApplication.class, args);
    }

}
