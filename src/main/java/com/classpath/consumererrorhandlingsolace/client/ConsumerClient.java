package com.classpath.consumererrorhandlingsolace.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;

@Component
@Slf4j
public class ConsumerClient {

    @Bean
    public Consumer<Message<String>> processMessage(){
        return (message) -> {
            log.info("Started processing the message:: {}", message.getPayload());
            final String flag = message.getHeaders().get("flag", String.class);
            if (flag.equals("true")){
                log.info("Successfully processed the message :: {}", message.getPayload());
            }else if (flag.equals("false")){
                log.error("Throwing IllegalArgumentException :: from the consumer");
                throw new IllegalArgumentException("Invalid payload");
            } else {
                log.error("Throwing IllegalStateException :: from the consumer");
                throw new IllegalStateException("Invalid payload");
            }
        };
    }

    @Bean
    public Function<String, Message<String>> messageProducer(){
        return (value) -> {
            if (value.equals("true")){
                System.out.println("Inside the success handler");
                return MessageBuilder.withPayload(UUID.randomUUID().toString() +" :::: "+ value).setHeader("flag", "true").build();
            }
            System.out.println("Inside the failure handler");
            return MessageBuilder.withPayload(UUID.randomUUID().toString()+" :::: "+value).setHeader("flag", "false").build();
        };
    }
}